import Vue from 'vue';
import VueRouter from "vue-router";
import Blackjack from "@/components/Blackjack";
import StartScreen from "@/components/StartScreen";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: "StartGame",
        component: StartScreen
    },
    {
        path: '/playgame',
        name: "Playstate",
        component: Blackjack
    },
]

const router = new VueRouter( {routes});

export default router;