export function fetchANewDeck() {
    return fetch('https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=6')
        .then(response => response.json())
}

export function drawANewCard(deckId) {
    return fetch(`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=1`)
        .then(response => response.json())
}

export function drawTwoCards(deckId) {
    return fetch(`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=2`)
        .then(response => response.json())
}